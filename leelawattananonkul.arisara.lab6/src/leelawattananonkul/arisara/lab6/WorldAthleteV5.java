package leelawattananonkul.arisara.lab6;
/*@Author Arisara Leelawattananonkul
 *2018-02-19
 *593040687-9
 */

import leelawattananonkul.arisara.lab5.BadmintonPlayer;

public class WorldAthleteV5 {
	public static void main(String[] args) {
		final int NUM_PLAYERS = 3;
		BadmintonPlayer[] players = new BadmintonPlayer[NUM_PLAYERS];
		players[0] = new ThaiBadmintonPlayer("Ratchanok Intanon", 
				55, 1.68, Gender.FEMALE, "05/02/1995", 66.5,  4);
		players[1] = new BadmintonPlayer("Tai Tzu-Ying", 
				57, 1.62, Gender.FEMALE, "Taiwan", "20/06/1994", 67.0,  1);
		players[2] = new ThaiBadmintonPlayer("Boonsak Ponsana", 
				72, 1.8, Gender.MALE, "22/02/1982", 70, 127);
		for (int i = 0; i < NUM_PLAYERS; i++) {
			System.out.println(players[i]);
			players[i].play();
		}
	}
}
