package leelawattananonkul.arisara.lab6;
/*@Author Arisara Leelawattananonkul
 *2018-02-19
 *593040687-9
 */

public class Book {
	private String name;
	private Author author;
	private double price;
	private int qnty;
	
	public Book(String name, Author author, double price) {
		this.name = name;
		this.author = author;
		this.price = price;
	}
	
	public Book(String name, Author author, double price, int qnty) {
		this.name = name;
		this.author = author;
		this.price = price;
		this.qnty = qnty;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getQnty() {
		return qnty;
	}

	public void setQty(int qty) {
		this.qnty = qnty;
	}

	@Override
	public String toString() {
		return "Book [ name = " + name + ", " + author + ", price = " + price + " qnty = " + qnty + " ]";
	}
	
}
