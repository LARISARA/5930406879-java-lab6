package leelawattananonkul.arisara.lab6;
/*@Author Arisara Leelawattananonkul
 *2018-02-19
 *593040687-9
 */
import leelawattananonkul.arisara.lab5.BadmintonPlayer;

public class ThaiBadmintonPlayer extends BadmintonPlayer {

	public ThaiBadmintonPlayer(String name, int weight, double height, Gender gender,
			String birthdate, double racketLength, int worldRanking) {
		super(name, weight, height, gender, "Thai", birthdate, racketLength, worldRanking);
		this.equipment = ("�١����");
	}
	
	@Override
	public void play() {
		super.play();
		System.out.println(getName() + " hits a " + this.equipment);
	}

}
