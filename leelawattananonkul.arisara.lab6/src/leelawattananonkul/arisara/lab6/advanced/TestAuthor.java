package leelawattananonkul.arisara.lab6.advanced;
/*@Author Arisara Leelawattananonkul
 *2018-02-19
 *593040687-9
 */

import leelawattananonkul.arisara.lab6.Author;

public class TestAuthor {
	
	public static void main(String[] args) {
		
		Author[] authors = new Author[2];
		authors[0] = new Author("James Gosling", "james@somewhere.com", 'm');
		authors[1] = new Author("Ken Arnold", "ken@nowhere.com", 'm');

		Book java = new Book("The Java programming language", authors, 19.99, 99);
		System.out.println(java);  
                        System.out.println("=== Author names of this book are ===");
		System.out.println(java.getAuthorNames());
	}
	
}