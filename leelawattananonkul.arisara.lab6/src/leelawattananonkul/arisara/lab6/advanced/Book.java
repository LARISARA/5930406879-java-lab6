package leelawattananonkul.arisara.lab6.advanced;
/*@Author Arisara Leelawattananonkul
 *2018-02-19
 *593040687-9
 */

import java.util.Arrays;
import leelawattananonkul.arisara.lab6.Author;

public class Book {
	
	private String name;
	private Author[] author;
	private double price;
	private int qnty;
	
	public Book(String name, Author[] author, double price) {
		this.name = name;
		this.setAuthor(author);
		this.price = price;
	}
	
	public Book(String name, Author[] author, double price, int qty) {
		this.name = name;
		this.setAuthor(author);
		this.price = price;
		this.qnty = qty;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getQty() {
		return qnty;
	}

	public void setQty(int qty) {
		this.qnty = qty;
	}

	public Author[] getAuthor() {
		return author;
	}

	public void setAuthor(Author[] author) {
		this.author = author;
	}
	
	public String getAuthorNames() {
		String name = author[0].getName();
		for (int i = 1; i < author.length; i++) {
			name += ", " + author[i].getName();
		}
		return name;
	}
	
	public void setAuthorNames(Author[] author) {
		this.setAuthor(author);
	}
	
	@Override
	public String toString() {
		return "Book [ name = " + name + ", \n"
				+ "author = " + Arrays.toString(author) + ", "
				+ " price = " + price + ", qnty = " + qnty + "]";
	}
	
	
}
