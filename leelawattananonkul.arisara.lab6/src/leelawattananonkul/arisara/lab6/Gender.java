package leelawattananonkul.arisara.lab6;
/*@Author Arisara Leelawattananonkul
 *2018-02-19
 *593040687-9
 */

public enum Gender {
	MALE, FEMALE
}
