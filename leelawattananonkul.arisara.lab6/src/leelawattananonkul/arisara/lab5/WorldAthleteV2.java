package leelawattananonkul.arisara.lab5;

/*@Author Arisara Leelawattananonkul
 *2018-02-02
 *593040687-9
 */

public class WorldAthleteV2 {

	public static void main(String[] args) {
		BadmintonPlayer ratchanok = new BadmintonPlayer("Ratchanok Intanon", 55.0, 1.68, Gender.FEMALE, "Thai",
				"05/02/1995", 66.5, 4);
		Footballer tom = new Footballer("Tom Brady", 102.0, 1.93, Gender.MALE, "American", "03/08/1977"
				, "Quarterback", "New England Patriots");
		Boxer wisaksil = new Boxer("Wisaksil Wangek", 51.5, 1.60, Gender.MALE, "Thai", "08/12/1986",
				"Super Flyweight", "M");

		System.out.println(ratchanok);
		System.out.println(wisaksil);
		System.out.println(tom);

		BadmintonPlayer nitchaon = new BadmintonPlayer("Nitchaon Jindapol", 52.0, 1.63, Gender.FEMALE, "Thailand",
				"31/03/1991", 67, 11);
		System.out.println(
				"Both " + ratchanok.getName() + " and " + nitchaon.getName() + " play " + BadmintonPlayer.getSport());

		ratchanok.compareAge(tom);
		ratchanok.compareAge(nitchaon);

		isTaller(wisaksil, tom);

	}

	public static void isTaller(Athlete player1, Athlete player2) {
		double height1 = player1.getHeight();
		double height2 = player2.getHeight();

		if (height1 > height2) {
			System.out.printf("%s is taller than %s", player1.getName(), player2.getName());
		}

		else if (height1 < height2) {
			System.out.printf("%s is taller than %s", player2.getName(), player1.getName());
		}

	}

}
