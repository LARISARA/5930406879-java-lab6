package leelawattananonkul.arisara.lab5;

/*@Author Arisara Leelawattananonkul
 *2018-02-02
 *593040687-9
 */

public class SuperBowl extends Competition {
	protected String AFCTeam;
	protected String NFCTeam;
	protected String winningTeam;

	public SuperBowl(String name, String date, String place, String description, String AFCTeam, String NFCTeam, String winningTeam) {
		super(name, date, place, description);
		this.AFCTeam = AFCTeam;
		this.NFCTeam = NFCTeam;
		this.winningTeam = winningTeam;
		
	}
	
	public String getAFCTeam() {
		return AFCTeam;
	}
	
	public void setAFCTeam(String aFCTeam) {
		AFCTeam = aFCTeam;
	}

	public String getNFCTeam() {
		return NFCTeam;
	}
	
	public void setNFCTeam(String nFCTeam) {
		NFCTeam = nFCTeam;
	}

	public String getWinningTeam() {
		return winningTeam;
	}
	
	public void setWinningTeam(String winningTeam) {
		this.winningTeam = winningTeam;
	}

	@Override
	public void setDescriptionAndRules() {
		System.out.printf("========= Begin : Description abd Rules ========= %n" 
				+ "SuperBowLII is played between the champions of the National Football Conference (NFC) %n"
				+ "and the American Football Conference (AFC). %n"
				+ "The game plays in four quarters while each quarter takes about 15 minutes. %n"
				+ "========= End : Description abd Rules ========= %n");
	}

	@Override
	public String toString() {
		return  name + "(" + description + ")" + " was held on " + date + " in " + place + "\n"
				+ "It was the game between " + AFCTeam + " vs. " + NFCTeam + "\n"
				+ "The winner was " + winningTeam;
	}
	
}
