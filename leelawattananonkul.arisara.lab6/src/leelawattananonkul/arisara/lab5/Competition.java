package leelawattananonkul.arisara.lab5;

/*@Author Arisara Leelawattananonkul
 *2018-02-02
 *593040687-9
 */

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public abstract class Competition {

	protected String name;
	protected LocalDate date;
	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
	protected String place;
	protected String description;

	public abstract void setDescriptionAndRules();

	public Competition(String name, String date, String place, String description) {
		super();
		this.name = name;
		this.date = LocalDate.parse(date, formatter);
		this.place = place;
		this.description = description;
	}

	public Competition(String description, String date, String place) {
		super();
		this.date = LocalDate.parse(date, formatter);
		this.place = place;
		this.description = description;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public LocalDate getDate() {
		return date;
	}
	
	public void setDate(String date) {
		this.date = LocalDate.parse(date, formatter);
	}
	
	public String getPlace() {
		return place;
	}
	
	public void setPlace(String place) {
		this.place = place;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}

}
