package leelawattananonkul.arisara.lab5;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

import leelawattananonkul.arisara.lab6.Gender;

public class BadmintonPlayer extends Athlete implements Playable, Movable {

	private String equipment = "Shuttlecock";
	protected static String sport;
	protected double racketLength;
	protected int worldRanking;


	public BadmintonPlayer(String name, double weight, double height, Gender gender, String nationality,
			String birthdate, double racketLength, int worldRanking) {
		super(name, weight, height, gender, nationality, birthdate);
		BadmintonPlayer.sport = "Badminton";
		this.racketLength = racketLength;
		this.worldRanking = worldRanking;
	}
	

	public static String getSport() {
		return sport;
	}

	public static void setSport(String sport) {
		BadmintonPlayer.sport = sport;
	}

	public double getRacketLength() {
		return racketLength;
	}

	public void setRacketLength(double racketLength) {
		this.racketLength = racketLength;
	}

	public int getWorldRanking() {
		return worldRanking;
	}

	public void setWorldRanking(int worldRanking) {
		this.worldRanking = worldRanking;
	}
	
	/**
	 * This method is to compare age of a given another athlete with this athlete. 
	 * To find the different in year between two LocalDate, use LocalDate dateBefore; 
	 * LocalDate dateAfter; int year = ChronoUnit.YEARS.between(dateBefore, dateAfter);
	 * @param player Another player to compare age with
	 */
	public void compareAge(Athlete player) {
		LocalDate dateBefore = super.getBirthdate(); 
		LocalDate dateAfter = player.getBirthdate();
		long year = ChronoUnit.YEARS.between(dateBefore, dateAfter);

		if (year < 0) {
			System.out.printf("%s is %d years older than %s %n", player.getName(), Math.abs(year), super.getName());
		}

		else if (year > 0) {
			System.out.printf("%s is %d years younger than %s %n", player.getName(), year, super.getName());
		}

	}
	
	@Override
	public void playSport() {
		System.out.println(getName() + " is good at " + getSport());
	}

	@Override
	public void move() {
		System.out.println(getName() + " moves around " + getSport().toLowerCase() + " court.");
		
	}

	public String getEquipment() {
		return equipment;
	}

	public void setEquipment(String equipment) {
		this.equipment = equipment;
	}
	
	public void play() {
		System.out.println(getName() + " hits a " + equipment);
	}
	
	@Override
	public String toString() {
		return "" + super.toString() + ", " + BadmintonPlayer.sport + ", " + racketLength + " cm, rank : "
				+ worldRanking + ", equipment : " + equipment; 
	}
	

}
