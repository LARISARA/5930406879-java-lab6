package leelawattananonkul.arisara.lab5;

/*@Author Arisara Leelawattananonkul
 *2018-02-02
 *593040687-9
 */

import java.time.format.DateTimeFormatter;
import java.time.LocalDate;

public class Athlete {

	protected String name;
	protected double weight;
	protected double height;
	protected Gender gender;
	protected String nationality;
	protected LocalDate birthdate;
	
	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

	public Athlete(String name, double weight, double height, Gender gender, String nationality, String birthdate) {
		super(); 
		this.name = name;
		this.weight = weight;
		this.height = height;
		this.gender = gender;
		this.nationality = nationality;
		this.birthdate = LocalDate.parse(birthdate, formatter);
	}

	public String getName(){
		return name;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public double getWeight(){
	return weight;
	}
	
	public void setWeight(double weight){
		this.weight = weight;
	}
	
	public double getHeight(){
		return height;
	}
	
	public void setHeight(double height){
		this.height = height;
	}
	
	public Gender getGender(){
		return gender;
	}
	
	public void setGender(Gender gender){
		this.gender = gender;
	}
	
	public String getNationality(){
		return nationality;
	}
	
	public void setNationality(String nationality){
		this.nationality = nationality;
	}
	
	public LocalDate getBirthdate(){
		return birthdate;
	}
	
	public void setBirthdate(String birthdate){
		this.birthdate = LocalDate.parse(birthdate, formatter);
	}
	
	@Override
	public String toString(){
		return "" + name + ", " + weight + " kg, " + height + " m, " + gender + ","
				+ " " + nationality + ", " + birthdate + "";
	}
		
	public void playSport(){
		System.out.println(getName() + " is good at sport");
	}
	
}
