package leelawattananonkul.arisara.lab5;

/*@Author Arisara Leelawattananonkul
 *2018-02-02
 *593040687-9
 */

public class ThailandGoTournament extends Competition {
	
	protected String winner;
	
	public ThailandGoTournament(String description, String date, String place) {
		super(description, date, place);
	}

	public String getWinner() {
		return winner;
	}
	
	public void setWinner(String winner) {
		this.winner = winner;
	}
	
	@Override
	public void setDescriptionAndRules() {
		System.out.printf("========= Begin : Description abd Rules =========%n"
				+ "The competition is open for all students and people %n"
				+ "The competition is divided into four categories %n"
				+ "1. High Dan (3 Dan and above) %n"
				+ "2. Low Dan (1-2 Dan) %n"
				+ "3. High Kyu (1-4 Kyu) %n"
				+ "4. Low Kyu (5-8 Kyu) %n"
				+ "========= End : Description abd Rules =========%n");
	}

	@Override
	public String toString() {
		return description + " was held on " + date + " in " + place + "." + "\n" 
				+ "Some winners are " + this.winner;
	}

}
