package leelawattananonkul.arisara.lab5;

/*@Author Arisara Leelawattananonkul
 *2018-02-02
 *593040687-9
 */

public interface Playable {
	public void play();
}
