package leelawattananonkul.arisara.lab5;

/*@Author Arisara Leelawattananonkul
 *2018-02-02
 *593040687-9
 */

public class Boxer extends Athlete implements Playable, Movable {

	private static String sport;
	private String division;
	private String golveSize;

	public Boxer(String name, double weight, double height, Gender gender, String nationality, String birthdate,
			String division, String golveSize) {
		super(name, weight, height, gender, nationality, birthdate);
		Boxer.sport = "Boxing";
		this.division = division;
		this.golveSize = golveSize;
	}

	public static String getSport() {
		return sport;
	}
	
	public static void setSport(String sport) {
		Boxer.sport = sport;
	}
	
	public String getDivision() {
		return division;
	}
	
	public void setDivision(String division) {
		this.division = division;
	}
	
	public String getGolveSize() {
		return golveSize;
	}
	
	public void setGolveSize(String golveSize) {
		this.golveSize = golveSize;
	}

	@Override
	public String toString() {
		return "" + super.toString() + ", " + Boxer.sport + ", " + division + ", " + golveSize + "";
	}	
	
	@Override
	public void playSport() {
		System.out.println(getName() + " is good at " + getSport());
	}
	
	@Override
	public void move() {
		System.out.println(getName() + " moves around a " + getSport().toLowerCase() + " ring.");		
	}
	
	@Override
	public void play() {
		System.out.println(getName() + " throws a punch.");
	}

}
